import icons from './assets/icons/font-awesome'
const environment = process.env.NUXT_ENV_TYPE || 'local'
const baseEnv = require(`./env/${environment}`)
const env = Object.assign({ environment }, baseEnv)
export default {
  env,
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'TRADING',
    htmlAttrs: {
      lang: 'ja'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicons.ico?v1', id: 'favicon' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    { src: '~/assets/scss/style.scss', lang: 'scss' },
    { src: 'vue-toastification/dist/index.css', lang: 'css' },
    { src: 'vue-select/dist/vue-select.css', lang: 'css' },
    { src: 'vue-file-agent/dist/vue-file-agent.css', lang: 'css' },
    { src: 'ag-grid-community/dist/styles/ag-grid.css', lang: 'scss' },
    { src: 'ag-grid-community/dist/styles/ag-theme-alpine.css', lang: 'scss' }
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/cheetah-core/cheetah-axios' },

    { src: '~/plugins/bootstrap-vue' },
    { src: '~/plugins/vue-i18n' },
    { src: '~/plugins/vee-validate' },
    { src: '~/plugins/vue-toastification' },
    { src: '~/plugins/moment' },
    { src: '~/plugins/vue-select' },
    { src: '@/plugins/vue-shortkey.js', mode: 'client' },
    // { src: '~/plugins/text-utils' },
    // { src: '~/plugins/text-mask' },
    // { src: '~/plugins/debugger' },
    // { src: '~/plugins/ck-editor' },
    { src: '~/plugins/vue-file-agent' },
    { src: '~/plugins/filters' },
    { src: '~/plugins/event-bus' },
    { src: '~/plugins/ag-grid-community' },
    { src: '~/plugins/vue-draggable' },
    { src: '~/plugins/vue-html2pdf' }

  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/eslint-module', // https://go.nuxtjs.dev/eslint
    '@nuxtjs/fontawesome',
    'cookie-universal-nuxt'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    // 'bootstrap-vue/nuxt',
    // '~/modules/user/index.js',
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    babel: {
      plugins: [['@babel/plugin-proposal-private-property-in-object', { loose: true }]]
    }
  },

  /*
   ** Nuxt Fontawesome
   ** See https://github.com/nuxt-community/fontawesome-module/
   */
  fontawesome: { icons },

  /*
   ** Active router link
   */
  router: {
    linkActiveClass: 'active-link'
  }
}
