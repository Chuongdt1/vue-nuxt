/**
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

import Vue from 'vue'
import { AgGridVue } from 'ag-grid-vue'

Vue.component('AgGridVue', AgGridVue)
