/**
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

import Vue from 'vue'
import vSelect from 'vue-select'

export default () => {
  Vue.component('VSelect', vSelect)
}
