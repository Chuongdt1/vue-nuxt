// /*
//  * Copyright 2021 DevFast Limited. All rights reserved.
//  * Email: tech@devfast.us .
//  */

import Vue from 'vue'
import VueFileAgent from 'vue-file-agent'

Vue.use(VueFileAgent)
