/**
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

import Vue from 'vue'
import Draggable from 'vuedraggable'

Vue.component('Draggable', Draggable)
