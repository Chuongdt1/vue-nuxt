import Vue from 'vue'
const ShortKey = require('vue-shortkey')

// add any custom shortkey config settings here
// Vue.use(ShortKey, { prevent: ['input', 'textarea'] })
Vue.use(ShortKey)

export default ShortKey
