/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

import Vue from 'vue'
import { ValidationObserver, ValidationProvider, extend } from 'vee-validate'
import {
  required,
  email,
  confirmed,
  regex,
  numeric
} from 'vee-validate/dist/rules'

extend('required', {
  ...required,
  message: (name) => {
    return parent.$nuxt.$t('messages.error.required', { name })
  }
})

extend('requiredPerson', {
  ...required,
  message: (name) => {
    return parent.$nuxt.$t('入力者IDを入力して下さい')
  }
})

extend('email', {
  ...email,
  message: () => {
    return parent.$nuxt.$t('messages.error.bad_email_format')
  }
})

extend('email_trimmed', {
  validate (value, { target }) {
    if (value) {
      // eslint-disable-next-line no-useless-escape
      const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      return regex.test(value.trim())
    } else {
      return false
    }
  },
  message: () => {
    return parent.$nuxt.$t('messages.error.bad_email_format')
  }
})

extend('confirmed', {
  ...confirmed,
  message: (name) => {
    return parent.$nuxt.$t('messages.error.unmatch_confirmation_email', { name })
  }
})

extend('regex', {
  ...regex,
  message: () => {
    return parent.$nuxt.$t('messages.error.invalid_format')
  }
})

extend('numeric', {
  ...numeric,
  message: () => {
    return parent.$nuxt.$t('messages.error.numeric')
  }
})

extend('numeric_trimmed', {
  validate (value, { target }) {
    if (value) {
      const regex = /^([0-9\uFF10-\uFF19]+)$/
      return regex.test(value.trim())
    } else {
      return false
    }
  },
  message: () => {
    return parent.$nuxt.$t('messages.error.numeric')
  }
})

extend('moreThan', {
  params: ['target', 'name'],
  validate (value, { target }) {
    return +value >= +target
  },
  message: (name, { target }) => {
    return parent.$nuxt.$t('messages.error.more_than', { name, target })
  }
})

extend('lessThan', {
  params: ['target', 'name'],
  validate (value, { target }) {
    return +value <= +target
  },
  message: (name, { target }) => {
    return parent.$nuxt.$t('messages.error.less_than', { name, target })
  }
})

extend('dateRange', {
  params: ['target'],
  validate (value, { target }) {
    return target && value ? (target >= value) : true
  },
  message: (name, { target }) => {
    return parent.$nuxt.$t('messages.error.date_before', { name, target })
  }
})

/**
 * Validate range of number
 */
extend('fieldRange', {
  params: ['range'],
  validate (value, { range }) {
    if (value.length && range) {
      if (range > value.trim().length || range < value.trim().length) {
        return false
      } else {
        return true
      }
    }
  },

  message: (name, { range }) => {
    return parent.$nuxt.$t('messages.error.invalid_range', { name, range })
  }
})

extend('temperature', {
  validate (value, { target }) {
    const regex = /^\d+(\.\d{1,1})?$/gim

    if (value && typeof value === 'number') {
      return Number(value) <= 42 && Number(value) >= 35
    } else if (value && regex.test(value.trim())) {
      return Number(value) <= 42 && Number(value) >= 35
    } else {
      return false
    }
  },
  message: (name) => {
    return parent.$nuxt.$t('messages.error.min_max', { name, min: '35°C', max: '42°C' })
  }
})

/**
 * Validate image
 */
extend('image', {
  validate (value) {
    if (value && value.length && value[0].type.match(/(jpg|jpeg|png|gif)$/)) {
      return true
    }
    return false
  },
  message: (name) => {
    return parent.$nuxt.$t('messages.error.invalid_image')
  }
})

/**
 * Validate max min
 */
extend('minmax', {
  validate (value, { min, max }) {
    if (value) {
      return value.trim().length >= min && value.trim().length <= max
    } else {
      return false
    }
  },
  message: (name, { min, max }) => {
    return parent.$nuxt.$t('messages.error.min_max', { name, min, max })
  },
  params: ['min', 'max']
})

/**
 * Validate min
 */
extend('min', {
  validate (value, { min }) {
    return value.length >= min
  },
  message: (name, { min }) => {
    return parent.$nuxt.$t('messages.error.min', { name, min })
  },
  params: ['min']
})

// validate haft size full size
extend('fullSize', {
  validate (value) {
    if ((/[ｧ-ﾝﾞﾟ]/.test(value))) {
      return false
    }
    return true
  },
  message: (name) => {
    return parent.$nuxt.$t('messages.error.fullSize', { name })
  },
  params: ['min']
})

extend('haftSize', {
  validate (value) {
    if ((/[ァ-ヶ０-９Ａ-ｚ]/.test(value))) {
      return false
    }
    return true
  },
  message: (name) => {
    return parent.$nuxt.$t('messages.error.haftSize', { name })
  },
  params: ['min']
})

extend('maxLengthFullSize', {
  validate (value, { max }) {
    // if (!(/[ァ-ヶ]/.test(value))) {
    //   return false
    // }
    return value.length <= max
  },
  message: (name, { max }) => {
    return parent.$nuxt.$t('messages.error.maxLengthFullSize', { name, max })
  },
  params: ['max']
})

extend('minLengthFullSize', {
  validate (value, { min }) {
    return value.length >= min
  },
  message: (name, { min }) => {
    return parent.$nuxt.$t('messages.error.minLengthFullSize', { name, min })
  },
  params: ['min']
})

extend('maxLengthHaftSize', {
  validate (value, { max }) {
    return String(value).trim().length <= max
  },
  message: (name, { max }) => {
    return parent.$nuxt.$t('messages.error.maxLengthHaftSize', { name, max })
  },
  params: ['max']
})

extend('minLengthHaftSize', {
  validate (value, { min }) {
    return value.length >= min
  },
  message: (name, { min }) => {
    return parent.$nuxt.$t('messages.error.minLengthHaftSize', { name, min })
  },
  params: ['min']
})

extend('minJapan', {
  validate (value, { min }) {
    if (!(/[ァ-ヶ]/.test(value))) {
      min = min * 2
    }
    return value.length >= min
  },
  message: (name, { min }) => {
    return parent.$nuxt.$t('messages.error.min', { name, min })
  },
  params: ['min']
})

extend('maxJapan', {
  validate (value, { max }) {
    if (!(/[ァ-ヶ]/.test(value))) {
      max = max * 2
    }
    return value.length <= max
  },
  message: (name, { max }) => {
    return parent.$nuxt.$t('messages.error.max', { name, max })
  },
  params: ['max']
})

/**
* Validate max line textarea
*/
extend('maxLine', {
  validate (value, { max }) {
    const rows = value.split('\n').length
    return rows <= max
  },
  message: (name, { max }) => {
    return parent.$nuxt.$t('messages.error.maxLine', { name, max })
  },
  params: ['max']
})

/**
* Validate max length per line textarea
*/
extend('maxLengthPerLine', {
  validate (value, { max }) {
    const rows = value.split('\n')
    for (let i = 0; i < rows.length; i++) {
      let lengthRow = 0
      for (let index = 0; index < rows[i].length; index++) {
        (/[ァ-ヶ０-９Ａ-ｚ]/.test(rows[i][index])) ? lengthRow += 2 : lengthRow += 1
        if (lengthRow > max) {
          return false
        }
      }
    }
    return true
  },
  message: (name, { max }) => {
    return parent.$nuxt.$t('messages.error.maxLengthPerLine', { name, max, maxFullWidth: max / 2 })
  },
  params: ['max']
})

/**
 * Validate color code
 */
extend('color-hex', {
  validate (value, { target }) {
    const regex = /^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/gim
    return regex.test(value.trim())
  },
  message: () => {
    return parent.$nuxt.$t('messages.error.color_hex')
  }
})

extend('password', {
  params: ['target'],
  validate (value, { target }) {
    return value === target
  },
  message: () => {
    return parent.$nuxt.$t('messages.error.unmatch_password')
  }
})

/**
 * Validate phone number
 *
 */
extend('fullNumber', {
  validate (value) {
    if (value) {
      // eslint-disable-next-line no-useless-escape
      const valueReplace = value.replace(/\(/g, '').replace(/\)/g, '').replace(/\-/g, '').replace(/\+/g, '')
      // eslint-disable-next-line no-useless-escape
      const regex = /^\d{10}$|^\d{20}|^[0-9０-９]+$/
      return regex.test(valueReplace.trim())
    } else {
      return false
    }
  },
  message: () => {
    return parent.$nuxt.$t('messages.error.numeric')
  }
})

/**
 * Validate Discount rate
 *
 */
extend('discountRate', {
  validate (value) {
    value = String(value)
    if (value) {
      // eslint-disable-next-line no-useless-escape
      const regex = /^[0-9０-９]\d{0,3}(\.\d{1})*(,\d+)?$/
      return regex.test(value)
    } else {
      return false
    }
  },
  message: (name) => {
    return parent.$nuxt.$t('messages.error.wrongDecimalFormat', { name })
  }
})
/**
 * Validate Number between
 *
 */
extend('between', {
  validate (value, { min, max }) {
    if (value) {
      return value >= parseInt(min) && value <= parseInt(max)
    } else {
      return false
    }
  },
  message: (name, { min, max }) => {
    return parent.$nuxt.$t('messages.error.min_max_value', { name, min, max })
  },
  params: ['min', 'max']
})

/**
 * Validate url
 */
extend('url', {
  validate (value, { target }) {
    const regex = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\\-\\-.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/
    return regex.test(value.trim())
  },
  message: () => {
    return parent.$nuxt.$t('messages.error.invalid_url')
  }
})

/**
 * Validate account ID
 */
extend('accountId', {
  validate (value) {
    const regex = /^[A-z]{3}[0-9]{4}$/
    return regex.test(value.trim())
  },
  message: () => {
    return parent.$nuxt.$t('messages.error.invalid_account')
  }
})

/**
 * Validate Domain Name
 */
extend('domain', {
  validate (value) {
    const regex = /^((?:(?:(?:\w[.\-+]?)*)\w)+)((?:(?:(?:\w[.\-+]?){0,62})\w)+)\.(\w{2,6})$/
    return regex.test(value.trim())
  },
  message: () => {
    return parent.$nuxt.$t('messages.error.bad_domain_format')
  }
})

extend('maxHalfSizeJp', {
  validate (value, { max }) {
    if ((/[ｧ-ﾝﾞﾟ]/.test(value))) {
      return value.length <= max
    } else if ((/[ァ-ヶ]/.test(value))) {
      return true
    }
    return value.length <= max
  },
  message: (name, { max }) => {
    return parent.$nuxt.$t('messages.error.maxLengthHaftSize', { name, max })
  },
  params: ['max']
})

extend('maxFullSizeJapan', {
  validate (value, { max }) {
    if ((/[ァ-ヶ]/.test(value))) {
      return value.length <= max
    }
    return true
  },
  message: (name, { max }) => {
    return parent.$nuxt.$t('messages.error.maxLengthFullSize', { name, max })
  },
  params: ['max']
})

extend('contracId', {
  validate (value, { max }) {
    // eslint-disable-next-line no-useless-escape
    if (/^[ァ-ヶa-z0-9A-Z_\(\)\.\+\/\#\s-]+$/.test(value)) {
      return value.length <= max
    }
    return false
  },
  message: (name, { max }) => {
    return 'コードには次の文字以外は使用できません。0~9,A~Z,a~z,スペース,_,#,-,+,.,/,(,)'
  },
  params: ['max']
})

extend('decimalNumber', {
  validate (value, { min, max }) {
    value = value.toString()
    if (value * 1 < 0) {
      return false
    } else if (value !== '') {
      if (typeof value === 'string' && value.includes(',')) {
        // eslint-disable-next-line no-useless-escape
        value = value.replace(/\,/g, '')
      }
      if (Number.isFinite(value * 1)) {
        if (value.includes('.') && max > 0) {
          const arr = value.split('.')
          if (arr.length === 2) {
            if (arr[1].length <= max && arr[0].length <= min) {
              return true
            }
          } return false
        } else if (value.length <= min && !value.includes('.')) {
          return true
        }
      }
    }
  },
  message: (name, { min, max }) => {
    return parent.$nuxt.$t('messages.error.decimalNumber', { min, max })
    // return parent.$nuxt.$t('messages.error.wrongDecimalFormat', { name })
  },
  params: ['min', 'max']
})

// Register it globally
Vue.component('ValidationObserver', ValidationObserver)
Vue.component('ValidationProvider', ValidationProvider)

export default (context, inject) => {
  inject('validator', ValidationProvider)
}
