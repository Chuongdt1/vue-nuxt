/**
 * Copyright 2019 S-cubism inc. All rights reserved.
 */

import { COLLAPSE_SIDEBAR, COLLAPSE_HIDE } from '~/constants/mutation-types'

/**
 * State
 */
export const state = () => ({
  isCollapsed: false,
  isHide: false
})

/**
 * Mutations
 */
export const mutations = {
  [COLLAPSE_SIDEBAR]: (state, isCollapsed) => {
    state.isCollapsed = isCollapsed
  },

  [COLLAPSE_HIDE]: (state, isHide) => {
    state.isHide = isHide
  }
}

/**
 * Getters
 */
export const getters = {
  isCollapsed: (state) => {
    return state.isCollapsed
  },

  isHide: (state) => {
    return state.isHide
  }
}

/**
 * Actions
 */
export const actions = {
  changeCollapse: ({ commit }, isCollapsed) => {
    commit(COLLAPSE_SIDEBAR, isCollapsed)
  },

  changeHide: ({ commit }, isHide) => {
    commit(COLLAPSE_HIDE, isHide)
  }
}

export default {
  state,
  mutations,
  getters,
  actions
}
