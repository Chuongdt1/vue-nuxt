import LocalStoreage from '~/cheetah-core/mixins/local-storeage'
async function CheckSecurityUser ({ store, redirect, app, route }) {
  const ADMIN = 'admin'
  const that = LocalStoreage.methods
  const userLogin = JSON.parse(that.getLocalStorage('user'))

  if (userLogin.UserId.toUpperCase() === ADMIN.toUpperCase()) {
    if (!route.path.includes('/section') && !route.path.includes('/user') && !route.path.includes('/menu')) {
      return redirect('/')
    }
  } else if (!userLogin.UserAdminId && userLogin.UserId.toUpperCase() !== ADMIN.toUpperCase()) {
    if (route.path.includes('/section') || route.path.includes('/user')) {
      return redirect('/')
    }
  } else {
    let searchMenu = null
    let security = null
    const routePath = route.path.split('/')
    const menu = await app.$cheetahAxios.getMenuByUserId({ userId: userLogin.UserId })
    if (menu) {
      searchMenu = menu.find(x => x.Path.toUpperCase() === routePath[1].toUpperCase())
    }
    const res = await app.$cheetahAxios.getSecurityByUserId({ userId: userLogin.UserId })
    if (res) {
      if (searchMenu) {
        security = res.data.find(s => s.MenuId === searchMenu.Id)
        if (security && security.Permission === 0) {
          if (!userLogin.UserAdminId && route.path.includes(routePath[1])) {
            return redirect('/')
          }
        }
      }
    }
  }
}

export default CheckSecurityUser
