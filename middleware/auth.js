/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

// import CONFIG from '~/plugins/dd-api-manager/config'

export default ({ store, redirect, app }) => {
  app.$cheetahAxios.setType('')

  const authToken = app.$cookies.get(app.$cheetahAxios.getCookieTokenName())
  if (!authToken) {
    return redirect('/login')
  }

  // Update cookies
  app.$cheetahAxios.setAuthToken(authToken)

  // app.$cheetahAxios.getUserInfo().then((res) => {
  //   store.dispatch('user/setUser', res.data)

  //   if (store.getters['user/isAdminSystem']) {
  //     app.$cheetahAxios.clearAuthToken()
  //     return redirect('/login')
  //   }
  // }).catch(() => {
  //   app.$cheetahAxios.clearAuthToken()
  //   return redirect('/login')
  // })

  // app.$cheetahAxios.getSystemSetting().then((res) => {
  //   store.dispatch('system-setting/setSystemSetting', res.data)
  // }).catch(() => {
  //   app.$cheetahAxios.clearAuthToken()
  //   return redirect('/login')
  // })
}
