// /* eslint-disable no-useless-escape */
// /*
//  * Copyright 2021 DevFast Limited. All rights reserved.
//  * Email: tech@devfast.us .
//  */
// import CONFIG from '~/plugins/dd-api-manager/config'

// // // eslint-disable-next-line space-before-function-paren
// export default async ({ store, redirect, app, route }) => {
//   const authToken = app.$cookies.get(CONFIG.LS_KEY_SESSION_TOKEN)
//   if (authToken) {
//     return redirect('/')
//   }

//   try {
//     await app.$cheetahAxios.getAdminInfo()
//     return redirect('/')
//   } catch (_) {
//     app.$cheetahAxios.clearAuthToken()
//   }
// }

export default ({ store, redirect, app, route }) => {
  app.$cheetahAxios.setType('')

  const authToken = app.$cookies.get(app.$cheetahAxios.getCookieTokenName())
  if (authToken) {
    return redirect('/')
  }
}
