/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

module.exports = {
  debug: true,

  local: true,

  // On/Off java script obfuscator
  isEncoderByObfuscator: false,

  // siteDomain: 'http://localhost:3031',

  webBaseUrl: 'http://http://localhost:3000/',

  cheetahAxios: {
    // baseApiURL: 'http://localhost:8000/api/',
    // baseImageURL: 'http://localhost:8000',
    // encodeCodition: true

    baseApiURL: 'http://211.5.219.5/api',
    isEncodeCodition: false,
    sessionTokenName: 'cheetah-',
    retryCount: 3,
    timeout: 30000
  }
}
