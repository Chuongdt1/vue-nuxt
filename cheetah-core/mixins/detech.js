/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

import UserInfoLogin from '~/cheetah-core/mixins/user-login-info'
import LocalStoreage from '~/cheetah-core/mixins/local-storeage'
if (window.performance.navigation && window.performance.navigation.type * 1 === 0) {
  if (localStorage.getItem('numberTabIsOpen') * 1 <= 0) {
    localStorage.removeItem('numberTabIsOpen')
  }
}
window.addEventListener('load', (event) => {
  if (localStorage.getItem('numberTabIsOpen') !== null) {
    localStorage.setItem('numberTabIsOpen', localStorage.getItem('numberTabIsOpen') * 1 + 1, { path: '/' })
  }
})
const Detech = {
  mixins: [
    LocalStoreage,
    UserInfoLogin
  ],

  created () {
    /**
     * If localStorage.getItem('numberTabIsOpen') doesn't exists
     *
     * @return 1
     */
    // if (await localStorage.getItem('numberTabIsOpen') === undefined || !localStorage.getItem('numberTabIsOpen')) {
    //   localStorage.setItem('numberTabIsOpen', 1, { path: '/' })
    // }

    /**
     * If paste url or new tab then Check if the number of open tabs is less than or equal 0
     * if true then clear session
     */
    if (window.performance.navigation && window.performance.navigation.type * 1 === 0) {
      if (localStorage.getItem('numberTabIsOpen') * 1 <= 0) {
        this.$cheetahAxios.clearAuthToken()
        this.removeUserLogin()
        window.location.reload()
      }
    }

    /**
     * event beforeunload occurs
     * Check if the number of open tabs is less than or equal 0
     * if true then clear session
     * if false then Number of open tabs minus 1
     */
    window.addEventListener('beforeunload', async (event) => {
      if (await localStorage.getItem('numberTabIsOpen') * 1 <= 0) {
        this.$cheetahAxios.clearAuthToken()
        this.removeUserLogin()
      } else {
        await localStorage.setItem('numberTabIsOpen', localStorage.getItem('numberTabIsOpen') * 1 - 1, { path: '/' })
      }
    })

    /**
     * event Load occurs
     * Number of open tabs plus 1
     */
    // await window.addEventListener('load', async (event) => {
    //   console.log(11111111111)
    //   await localStorage.setItem('numberTabIsOpen', localStorage.getItem('numberTabIsOpen') * 1 + 1, { path: '/' })
    // })
  }
}

export default Detech
