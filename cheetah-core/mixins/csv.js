/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

import Encoding from 'encoding-japanese'
import Papa from 'papaparse'

export default {
  methods: {
  /**
   * Parse and encode txt data before export and download txt file
   */
    parseAndDowloadTxt (dataTarget, fileName, charset, field = []) {
      const processRow = function (row) {
        // eslint-disable-next-line no-unused-vars
        let finalVal = ''
        let titleHasSpace = ''
        // eslint-disable-next-line no-unused-vars
        const title = []
        let k = 0
        Object.keys(row).forEach((field) => {
          let innerValue = row[field] === null ? '' : row[field].toString()
          if (row[field] instanceof Date) {
            innerValue = row[field].toLocaleString()
          };
          let result = innerValue
          if (result.search(/("|,|\n)/g) >= 0) {
            result = '"' + result + '"'
          }
          if (title.includes(field) === false) {
            title[k] = field
            k = k + 1
          }
          finalVal += result
          finalVal += '   '
        })
        for (let index = 0; index < title.length; index++) {
          titleHasSpace += title[index]
          // eslint-disable-next-line no-unused-vars
          titleHasSpace += '    '
        }
        // return {
        //   title: titleHasSpace + '\n',
        //   value: finalVal + '\n'
        // }
        return finalVal + '\n'
      }

      const processRowTitle = function (row, fieldList = [], isManual = false) {
        // eslint-disable-next-line no-unused-vars
        let titleHasSpace = ''
        // eslint-disable-next-line no-unused-vars
        let title = []

        if (!isManual) {
          let k = 0
          Object.keys(row).forEach((field) => {
            if (title.includes(field) === false) {
              title[k] = field
              k = k + 1
            }
          })
        } else {
          title = fieldList
        }

        for (let index = 0; index < title.length; index++) {
          titleHasSpace += title[index]
          // eslint-disable-next-line no-unused-vars
          titleHasSpace += '    '
        }
        // return {
        //   title: titleHasSpace + '\n',
        //   value: finalVal + '\n'
        // }
        return titleHasSpace + '\n'
      }

      const txtTitle = processRowTitle(dataTarget[0], field, (field.length !== 0))

      let txtContent = ''
      // txtContent[0] = processRow(dataTarget[0]).title
      for (let i = 0; i < dataTarget.length; i++) {
        txtContent += processRow(dataTarget[i])
      }

      txtContent = txtTitle + txtContent

      // const csv = Papa.unparse({
      //   data: txtContent
      // })
      // const uniArray = Encoding.stringToCode(csv)
      // const unicodeString = Encoding.convert(uniArray, {
      //   to: charset,
      //   from: 'UNICODE'
      // })
      // const unit8Array = new Uint8Array(txtContent)
      const blob = new Blob([txtContent], { type: 'text/csv;charset=' + charset })
      const link = document.createElement('a')
      const url = (window.URL || window.webkitURL).createObjectURL(blob)
      link.setAttribute('href', url)
      link.setAttribute('charset', charset)
      link.setAttribute('download', fileName)
      link.style.visibility = 'hidden'
      document.body.appendChild(link)
      link.click()
      setTimeout(() => { (window.URL || window.webkitURL).revokeObjectURL(link.href) }, 100)
      document.body.removeChild(link)
      // this.parseAndDowloadCsv(txtContent, fileName, charset)
    },

    /**
   * Parse and encode csv data before export and download csv file [old]
   */
    parseAndDowloadCsvOLD (dataTarget, fileName, charset, field = []) {
      let csv = null

      // Check create fields automatically or manually
      if (field.length === 0) {
        csv = Papa.unparse({
          data: dataTarget
        })
      } else {
        const dataTargetArray = dataTarget.map(function (obj) {
          return Object.values(obj)
        })
        csv = Papa.unparse({
          fields: field,
          data: dataTargetArray
        })
      }
      const uniArray = Encoding.stringToCode(csv)

      // if (format === 0 || !format) {
      //   charset = 'Shift_JIS'
      //   sjisArray = Encoding.convert(uniArray, {
      //     to: 'SJIS',
      //     from: 'UNICODE'
      //   })
      // } else if (format === 1) {
      //   charset = 'UTF-16LE'
      //   sjisArray = Encoding.convert(uniArray, {
      //     to: 'UTF-16LE',
      //     from: 'UNICODE'
      //   })
      // }

      const convertToCharset = Encoding.convert(uniArray, {
        to: charset,
        from: 'UNICODE'
      })

      const unit8Array = new Uint8Array(convertToCharset)
      const blob = new Blob([unit8Array], { type: 'text/csv;charset=' + charset })
      const link = document.createElement('a')
      // Browsers that support HTML5 download attribute
      const url = (window.URL || window.webkitURL).createObjectURL(blob)
      link.setAttribute('href', url)
      link.setAttribute('charset', charset)
      link.setAttribute('download', fileName)
      link.style.visibility = 'hidden'
      document.body.appendChild(link)
      link.click()
      setTimeout(() => { (window.URL || window.webkitURL).revokeObjectURL(link.href) }, 100)
      document.body.removeChild(link)
    },

    /**
   * Parse and encode txt data before export and download txt file
   */
    // parseAndDowloadCsv (dataTarget, fileName, charset, field = []) {
    //   const processRow = function (row) {
    //     // eslint-disable-next-line no-unused-vars
    //     let finalVal = ''
    //     let titleHasSpace = ''
    //     // eslint-disable-next-line no-unused-vars
    //     const title = []
    //     let k = 0
    //     Object.keys(row).forEach((field) => {
    //       let innerValue = row[field] === null ? '' : row[field].toString()
    //       if (row[field] instanceof Date) {
    //         innerValue = row[field].toLocaleString()
    //       };
    //       let result = innerValue
    //       if (result.search(/("|,|\n)/g) >= 0) {
    //         result = '"' + result + '"'
    //       }
    //       if (title.includes(field) === false) {
    //         title[k] = field
    //         k = k + 1
    //       }
    //       finalVal += result
    //       finalVal += ','
    //     })
    //     for (let index = 0; index < title.length; index++) {
    //       titleHasSpace += title[index]
    //       // eslint-disable-next-line no-unused-vars
    //       titleHasSpace += '    '
    //     }
    //     // return {
    //     //   title: titleHasSpace + '\n',
    //     //   value: finalVal + '\n'
    //     // }
    //     return finalVal + '\n'
    //   }

    //   const processRowTitle = function (row, fieldList = [], isManual = false) {
    //     // eslint-disable-next-line no-unused-vars
    //     let titleHasSpace = ''
    //     // eslint-disable-next-line no-unused-vars
    //     let title = []

    //     if (!isManual) {
    //       let k = 0
    //       Object.keys(row).forEach((field) => {
    //         if (title.includes(field) === false) {
    //           title[k] = field
    //           k = k + 1
    //         }
    //       })
    //     } else {
    //       title = fieldList
    //     }

    //     for (let index = 0; index < title.length; index++) {
    //       titleHasSpace += title[index]
    //       // eslint-disable-next-line no-unused-vars
    //       titleHasSpace += ','
    //     }
    //     // return {
    //     //   title: titleHasSpace + '\n',
    //     //   value: finalVal + '\n'
    //     // }
    //     return titleHasSpace + '\n'
    //   }

    //   const txtTitle = processRowTitle(dataTarget[0], field, (field.length !== 0))

    //   let txtContent = ''
    //   // txtContent[0] = processRow(dataTarget[0]).title
    //   for (let i = 0; i < dataTarget.length; i++) {
    //     txtContent += processRow(dataTarget[i])
    //   }

    //   txtContent = txtTitle + txtContent
    //   // const csv = Papa.unparse({
    //   //   data: txtContent
    //   // })
    //   // const uniArray = Encoding.stringToCode(csv)
    //   // const unicodeString = Encoding.convert(uniArray, {
    //   //   to: charset,
    //   //   from: 'UNICODE'
    //   // })
    //   // const unit8Array = new Uint8Array(txtContent)
    //   const blob = new Blob([txtContent], { type: 'text/csv' })
    //   const link = document.createElement('a')
    //   const url = (window.URL || window.webkitURL).createObjectURL(blob)
    //   link.setAttribute('href', url)
    //   link.setAttribute('charset', charset)
    //   link.setAttribute('download', fileName)
    //   link.style.visibility = 'hidden'
    //   document.body.appendChild(link)
    //   link.click()
    //   setTimeout(() => { (window.URL || window.webkitURL).revokeObjectURL(link.href) }, 100)
    //   document.body.removeChild(link)
    //   // this.parseAndDowloadCsv(txtContent, fileName, charset)
    // },

    parseAndDowloadCsv (dataTarget, fileName, charset, field = []) {
      const value = []
      for (let index = 0; index < dataTarget.length; index++) {
        value.push(Object.values(dataTarget[index]))
      }
      const csv = Papa.unparse({
        fields: field,
        data: value
      }, {
        quotes: true, // or array of booleans
        quoteChar: '"',
        escapeChar: '"',
        delimiter: ','
        // header: true,
        // newline: '\r\n',
        // skipEmptyLines: false, // other option is 'greedy', meaning skip delimiters, quotes, and whitespace.
        // columns: null // or array of strings
      })
      const uniArray = Encoding.stringToCode(csv)
      const sjisArray = Encoding.convert(uniArray, {
        to: 'SJIS',
        from: 'UNICODE'
      })
      const unit8Array = new Uint8Array(sjisArray)
      const blob = new Blob([unit8Array], { type: 'text/csv;charset=' + charset })
      const link = document.createElement('a')
      // Browsers that support HTML5 download attribute
      const url = (window.URL || window.webkitURL).createObjectURL(blob)
      link.setAttribute('href', url)
      link.setAttribute('charset', charset)
      link.setAttribute('download', fileName)
      link.style.visibility = 'hidden'
      document.body.appendChild(link)
      link.click()
      setTimeout(() => { (window.URL || window.webkitURL).revokeObjectURL(link.href) }, 100)
      document.body.removeChild(link)
    }
  }
}
