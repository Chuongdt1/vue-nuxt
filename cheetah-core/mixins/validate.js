/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

const Validate = {
  methods: {
    /**
      * check validate limit maxlength with fullwidth or haftwidth
      */
    limitMaxLength (value, evt) {
      const valueInput = value
      evt = (evt) || window.event
      let lengthValueInput = 0
      let index = 0
      for (let i = 0; i < valueInput.length; i++) {
        (/[ァ-ヶ０-９Ａ-ｚ]/.test(valueInput[i])) ? lengthValueInput += 2 : lengthValueInput += 1
        index = i
        if (lengthValueInput > evt.target.maxLength) {
          break
        }
      }
      if (lengthValueInput > evt.target.maxLength) {
        return valueInput.substr(0, index)
      }
      return value
    },

    /**
    * check validate limit maxlength Decimal
    */
    limitDecimal (value, evt) {
      const valueInput = value
      evt = (evt) || window.event
      let lengthValueInput = 0

      const tmp = valueInput.split('.')
      lengthValueInput = tmp.length > 1 ? tmp[1].length : 0
      if (lengthValueInput > 1) {
        const index = tmp[0].length + 1 + 1
        return valueInput.substr(0, index)
      }

      lengthValueInput = tmp.length > 0 ? tmp[0].length : 0
      if (lengthValueInput > 4) {
        const index = 4
        return valueInput.substr(0, index)
      }

      return value
    },

    /**
      * Limit Max Length Per Row
      */
    limitMaxLengthPerRow (value, evt) {
      const valueInput = value
      evt = (evt) || window.event
      let listRows = valueInput.split('\n')
      const maxLengthPerLine = evt.target.attributes.maxlengthperline.value

      listRows = listRows.map((row) => {
        let index = 0
        let lengthValueInput = 0

        for (let i = 0; i < row.length; i++) {
          (/[ァ-ヶ０-９Ａ-ｚ]/.test(row[i])) ? lengthValueInput += 2 : lengthValueInput += 1
          index = i
          if (lengthValueInput > maxLengthPerLine) {
            break
          }
        }

        if (lengthValueInput > maxLengthPerLine) {
          return row.substr(0, index)
        }

        return row
      })

      return listRows.join('\n')
    },

    /*
      * check validate limit maxlength with maxlength fullwidth = maxlength haftwidth
      */
    limitMaxLengthJP (value, evt) {
      const valueInput = value
      evt = (evt) || window.event
      return valueInput.substr(0, evt.target.maxLength)
    }
  }
}

export default Validate
