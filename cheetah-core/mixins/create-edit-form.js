/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

const EVENT_MODIFY = 'modify'

const CreateEditForm = {
  props: {
    /**
     * Model id
     */
    id: {
      type: Number,
      default: 0
    }
  },

  data () {
    return {
      loading: false,
      idCurrent: 0
    }
  },

  methods: {
    /**
     * Get item detail
     *
     * @param {Number} id
     */
    getDetail (id) {
      this.loading = true
      if (id) {
        const action = this.model.detailAction

        this.$cheetahAxios[action]({ id })
          .then((res) => {
            this.setModel(res.data)
          })
          .catch((err) => {
            console.error(err)

            this.$toast.error(
              this.$t('messages.error.failed_to_get', { name: this.modelType })
            )
          }).finally(() => {
            this.loading = false
          })
      } else {
        this.setModel()
        this.loading = false
      }
    },

    /**
     * Event trigger on Submit
     */
    async onSubmit () {
      this.loading = true
      try {
        const formData = this.model.getFormData()
        let action = null
        if (this.idCurrent) {
          action = this.idCurrent ? this.model.updateAction : this.model.createAction
        } else {
          action = this.id ? this.model.updateAction : this.model.createAction
        }

        await this.$cheetahAxios[action](formData)

        this.$toast.success(
          this.$t(
            this.id ? 'messages.information.updated' : 'messages.information.created',
            { name: this.modelType }
          )
        )

        this.$emit(EVENT_MODIFY)
      } catch (err) {
        console.error(err)

        const message = err?.response?.data?.messages || ''

        if (message) {
          this.$toast.error(message)
        } else {
          this.$toast.error(
            this.$t(
              this.id ? 'messages.error.failed_to_update' : 'messages.error.failed_to_create',
              { name: this.modelType }
            )
          )
        }
      } finally {
        this.loading = false
      }
    }

  }
}

export default CreateEditForm
