const Round = {
  methods: {
    /**
    * Set User login from local storage
    */
    roundNumber (number, element, CryID, keyObject) {
      let el = 2
      if (typeof element !== 'object') {
        el = element
      } else if (CryID && keyObject && keyObject !== '') {
        if (CryID * 1 === 0) {
          keyObject = keyObject + 'US'
        } else {
          keyObject = keyObject + 'JP'
        }
        el = element[keyObject]
      }
      if (typeof number === 'string') {
        // return (number * 1).toFixed(element)
        return parseFloat(number).toFixed(el * 1)
      } else if (typeof number === 'number') {
        return number.toFixed(el * 1)
      }
    }
  }
}

export default Round
