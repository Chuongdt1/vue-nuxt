/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

const AddOrRemoveDisable = {
  methods: {
    /**
     * add disable
     */
    preventControl (idHtml) {
      if (document.getElementById(idHtml)) {
        const items = document.getElementsByClassName('order-item')
        for (let i = 0; i < items.length; i++) {
          items[i].setAttribute('disabled', true)
        }
      }
    },

    /**
     * Remove disable
     */
    continuteControl () {
      const items = document.getElementsByClassName('order-item')
      const keyboarderID = document.getElementById('keyboarderID')
      if (keyboarderID) {
        keyboarderID.removeAttribute('disabled')
      }
      for (let i = 0; i < items.length; i++) {
        items[i].removeAttribute('disabled')
      }
    }
  }
}

export default AddOrRemoveDisable
