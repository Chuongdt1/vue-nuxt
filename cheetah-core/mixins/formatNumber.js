const formatNumber = {
  methods: {
    /**
    * format number
    */
    formatNumber (n, element, CryID, keyObject) {
      const int = n * 1
      let el = 2
      if (typeof element !== 'object') {
        el = element
      } else if (CryID && keyObject && keyObject !== '') {
        if (CryID * 1 === 0) {
          keyObject = keyObject + 'US'
        } else {
          keyObject = keyObject + 'JP'
        }
        el = element[keyObject]
      }
      try {
        if (el * 1 === 0) {
          return (int.toString() + '.').replace(/\d(?=(\d{3})+\.)/g, '$&,').replace('.', '')
        }
        return int.toFixed(el).replace(/\d(?=(\d{3})+\.)/g, '$&,')
      } catch (error) {
        return n
      }
    }
  }
}

export default formatNumber
