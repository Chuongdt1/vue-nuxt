/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */
import axios from 'axios'

const DownloadFile = {
  methods: {
    /**
     * Download file
     *
     * @param {String} url - Url
     * @param {String} fileName - File name
     * @param {String} fileType - File type
     */
    downloadFile (url, fileName, fileType) {
      axios.get(url, { responseType: 'blob' })
        .then((response) => {
          const blob = new Blob([response.data], { type: fileType })
          const link = document.createElement('a')
          link.href = URL.createObjectURL(blob)
          link.download = fileName
          link.click()
          URL.revokeObjectURL(link.href)
        })
        .catch((err) => {
          console.error(err)
        })
    }
  }
}

export default DownloadFile
