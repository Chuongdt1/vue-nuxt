/**
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

export const mockManager = {
  getData: async (api, params, options) => {
    const moduleName = api.useMock
    const mockData = await import('~/mock-data/' + moduleName + '.js')
    const data = mockData.default[api.operationId](params, options)

    return data
  }
}
