/**
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

export default {
  getUserList: () => {
    return {
      data: [
        {
          id: 1,
          KeyboarderID: 'A2333',
          KeyboarderName: 'rory41@gmail.com',
          SectionID: '00001'
        },
        {
          id: 2,
          KeyboarderID: 'A2334',
          KeyboarderName: 'okee@gmail.com',
          SectionID: '00001'
        },
        {
          id: 3,
          KeyboarderID: 'A2335',
          KeyboarderName: 'haha@gmail.com',
          SectionID: '00001'
        },
        {
          id: 4,
          KeyboarderID: 'A2336',
          KeyboarderName: 'hhihih@gmail.com',
          SectionID: '00001'
        }
      ],
      links: {
        first: 'http://content-management-api.test/api/user?page=1',
        last: 'http://content-management-api.test/api/user?page=11',
        prev: null,
        next: 'http://content-management-api.test/api/user?page=2'
      },
      meta: {
        current_page: 1,
        from: 1,
        last_page: 11,
        path: 'http://content-management-api.test/api/user',
        per_page: 10,
        to: 10,
        total: 101
      }
    }
  },

  createUser: () => {
    return {}
  },

  getUser: () => {
    return {
      data: {
        id: 4,
        KeyboarderID: 'A2336',
        KeyboarderName: 'hhihih@gmail.com',
        SectionID: '00001'
      }
    }
  },

  updateUser: () => {
    return {
      data: {
        api_token: null,
        ebay_authenticated: false,
        email: 'mann.gracie@trantow.biz',
        id: 101,
        name: 'Test Mock Lyric Connelly MD',
        paypal_authenticated: false,
        roles: [],
        status: 1
      }
    }
  },

  deleteUser: () => {
    return {}
  }
}
