import { get } from 'lodash'

export default class User {
  constructor (props) {
    this.Id = Number(get(props, 'Id', 0))
    this.KeyboarderID = get(props, 'KeyboarderID', '')
    this.KeyboarderName = get(props, 'KeyboarderName', '')
    this.SectionID = get(props, 'SectionID', '')
    this.PassWord = get(props, 'PassWord', '')
    this.PassWordExpir = Number(get(props, 'PassWordExpir', 30))
    this.ExpriationDate = get(props, 'ExpriationDate', '')
    this.SecMenu01 = get(props, 'SecMenu01', '')
    this.ApprovalAltOK = get(props, 'ApprovalAltOK', '00')
    this.ExceedSectionOK = get(props, 'ExceedSectionOK', '00')
    this.SeqNoColumnNo = Number(get(props, 'SeqNoColumnNo', 0))
    this.UserName = get(props, 'UserName', '')
    this.MailAddress = get(props, 'MailAddress', '')
    this.PopBeforSmtp = Number(get(props, 'PopBeforSmtp', 0))
    this.PopAddress = get(props, 'PopAddress', '')
    this.PopPort = Number(get(props, 'PopPort', 0))
    this.PopAccount = get(props, 'PopAccount', 0)
    this.PopPassword = get(props, 'PopPassword', '')
    this.SmtpAddress = get(props, 'SmtpAddress', '')
    this.SmtpPort = Number(get(props, 'SmtpPort', 0))
    this.SmtpAccount = get(props, 'SmtpAccount', '')
    this.SmtpPassword = get(props, 'SmtpPassword', 0)
    this.SmtpCertification = Number(get(props, 'SmtpCertification', 0))
    this.SmtpSSL = Number(get(props, 'SmtpSSL', 0))
    this.MailEncodingID = get(props, 'MailEncodingID', null)
    this.ServerOK = Number(get(props, 'ServerOK', 0))
    this.NewRegisterDate = get(props, 'NewRegisterDate', '')
    this.LastDateUpDated = get(props, 'IsBulkDiscount', '')
    this.NewRegisterKeyboarder = get(props, 'WhereTo', '0')
    this.LastKeyboarderUpDated = get(props, 'LastKeyboarderUpDated', '')

    // Action
    this.createAction = 'createUser'
    this.detailAction = 'getUser'
    this.updateAction = 'updateUser'
  }

  /**
   * Get form data
   */
  getFormData () {
    const newFormData = {
      KeyboarderID: this.KeyboarderID,
      KeyboarderName: this.KeyboarderName,
      SectionID: this.SectionID,
      PassWord: this.PassWord,
      PassWordExpir: this.PassWordExpir,
      ExpriationDate: this.ExpriationDate,
      SecMenu01: this.SecMenu01,
      ApprovalAltOK: this.ApprovalAltOK,
      ExceedSectionOK: this.ExceedSectionOK,
      SeqNoColumnNo: this.SeqNoColumnNo,
      UserName: this.UserName,
      MailAddress: this.MailAddress,
      PopBeforSmtp: this.PopBeforSmtp,
      PopAddress: this.PopAddress,
      PopPort: this.PopPort,
      PopAccount: this.PopAccount,
      PopPassword: this.PopPassword,
      SmtpAddress: this.SmtpAddress,
      SmtpPort: this.SmtpPort,
      SmtpAccount: this.SmtpAccount,
      SmtpPassword: this.SmtpPassword,
      SmtpCertification: this.SmtpCertification,
      SmtpSSL: this.SmtpSSL,
      MailEncodingID: this.MailEncodingID,
      ServerOK: this.ServerOK,
      NewRegisterDate: this.NewRegisterDate,
      LastDateUpDated: this.LastDateUpDated,
      NewRegisterKeyboarder: this.NewRegisterKeyboarder,
      LastKeyboarderUpDated: this.LastKeyboarderUpDated
    }

    if (this.type && this.type.includes('1')) {
      newFormData.IsSensitive = true
    } else {
      newFormData.IsSensitive = false
    }

    if (this.Id) {
      newFormData.Id = this.Id
    }

    return newFormData
  }
}
