/**
 * Company TYPE_SEARCH list
 */
export const TYPE_SEARCH = [
  {
    text: '頭出検索',
    value: '0'
  },
  {
    text: '大文字小文字を区別しない',
    value: '1'
  }
]
