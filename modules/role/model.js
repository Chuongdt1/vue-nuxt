/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

import { get } from 'lodash'

// import Permission from './Permission'

export default class Role {
  constructor (props) {
    this.id = get(props, 'id', 0)
    this.name = get(props, 'name', '')
    this.code = get(props, 'id', 0) // For v-select on form
    this.label = get(props, 'name', '') // For v-select on form
    // this.permissions = get(props, 'permissions', []).map(item => new Permission(item))

    // Action
    this.createAction = 'createRole'
    this.detailAction = 'getRole'
    this.updateAction = 'updateRole'
  }

  /**
   * Get form data
   */
  getFormData () {
    return {
      id: this.id,
      name: this.name
      // permissions: this.permissions.map(item => item.id)
    }
  }
}
