
// import { resolve } from 'path'// index.js
module.exports = function RoleModule (moduleOptions) {
  this.extendRoutes((routes) => {
    routes.unshift({
      name: 'role-index',
      path: '/role',
      // component: resolve(themeDir, 'pages/Role.vue')
      component: '~/modules/role/pages/Role.vue'
    })

    // routes.unshift({
    //   name: 'role',
    //   path: '/role/',
    //   // component: resolve(themeDir, 'pages/Role.vue')
    //   component: '~/modules/role/pages/Role.vue'
    // })

    routes.unshift({
      name: 'role-create',
      path: '/role/create',
      // component: resolve(themeDir, 'pages/Role.vue')
      component: '~/modules/role/pages/RoleCreate.vue'
    })

    // routes.unshift({
    //   name: 'role-edit',
    //   path: '/role/edit/:slug/',
    //   // component: resolve(themeDir, 'pages/Role.vue')
    //   component: '~/modules/role/pages/RoleEdit.vue'
    // })

    routes.unshift({
      name: 'role-search',
      path: '/role/search/:slug/',
      // component: resolve(themeDir, 'pages/Role.vue')
      component: '~/modules/role/pages/RoleSearch.vue'
    })
  }
  )
  // we can't register stores through Nuxt modules so we have to make a plugin
  // this.addPlugin(path.resolve(__dirname, 'plugins/add-stores.js'))
}
