/**
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 *
 * Rules of define operationId CRUD API
 *
 * Main: GET LIST
 * Rule: get{ModuleName}List
 * Example: 'getUserList'
 *
 * Main: POST
 * Rule: create{module_name}
 * Example: 'createUser'
 *
 * Main: GET DETAIL
 * Rule: get{module_name}
 * Example: 'getUser'
 *
 * Main: PUT
 * Rule: update{module_name}
 * Example: 'updateUser'
 *
 * Main: DELETE
 * Rule: delete{module_name}
 * Example: 'deleteUser'
 */

export const SwaggerApi = {
  paths: {
    // Role API
    '/role': {
      post: {
        operationId: 'createRole',
        useMock: 'role'
      },
      get: {
        operationId: 'getRoleList',
        useMock: 'role'
      }
    },
    '/role/{id}': {
      get: {
        operationId: 'getRole',
        useMock: 'role'
      },
      put: {
        operationId: 'updateRole',
        useMock: 'role'
      },
      delete: {
        operationId: 'deleteRole',
        useMock: 'role'
      }
    }
  }
}
