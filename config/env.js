/*
 * Copyright 2020 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

const environment = process.env.NODE_ENV || 'local'
const baseEnv = require(`../env/${environment}`)
const env = Object.assign({ environment }, baseEnv)

module.exports = env
